<!DOCTYPE html>
<html>
	<head>
		<title>Home Page</title>
	</head>
	<body>
	<div class="container box">
			<h3 align="center">Home Page</h3>
		@if(isset(Auth::user()->email))
			<div class="alert alert-danger success-block">
				<strong>Welcome {{Auth::user()->name}}</strong>
				<br/>
				<a href = "/login/logout">Logout</a>
			<div>
		@else
			<script>window.location="/login";</script>
		@endif
		<div align="center" class = "row">
			<div class="col-md-12">
				<br/>
				<h3> My Files</h3>
				<br/>
				@foreach($fileData as $file)
					<li>
						<a href="{{route('downloadfile', $file['id'])}}">
							{{$file['file_name']}}
						</a>
					</li>
				@endforeach
			</div>
		</div>
	</div>
	</body>

</html>