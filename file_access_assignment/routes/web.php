<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect("/", "/login");

Route::get("/login", "LoginController@index");

Route::post("login/checkLoginRequest", "LoginController@checkLoginRequest");

Route::get("login/successLogin", "LoginController@successLogin");

Route::get("login/logout", "LoginController@logout");

Route::get("/home", "HomeController@index");

Route::get('download/{id}', 'HomeController@downloadFile')->name('downloadfile');
