<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\FilePermissions;
use App\User;

class HomeController extends Controller
{
	 public function index( Request $request, User $userClass){
		$mail = Auth::user()->email;
		$fileData = FilePermissions::all()->where('accessed', $mail);
        return view('home', compact('fileData'));
    }

	public function downloadFile($id){
		$fileData = FilePermissions::find($id);
		$fileName = $fileData['file_name'];
		$pdf_contents = $fileData['file_data'];
		file_put_contents($fileName, $pdf_contents);
		header('Content-type: application/pdf');
		header("Cache-Control: no-cache");
		header("Pragma: no-cache");
		header("Content-Disposition: inline;filename='".$fileName);
		header("Content-length: ".strlen($pdf_contents));

		echo $pdf_contents;
	}
}
