<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
			'name'	=>	'User1',
			'email'	=>	'user_one@gmail.com',
			'password'=>	Hash::make('password'),
			'remember_token'=>str_random(10),
		]);
		
		User::create([
			'name'	=>	'User2',
			'email'	=>	'user_two@gmail.com',
			'password'=>	Hash::make('password'),
			'remember_token'=>str_random(10),
		]);
    }
}
